package sheridan;


public class EmailValidator 
{

	public static boolean isValidEmail(String email) throws NullPointerException
	{		
    int domainLength = email.lastIndexOf('.') - email.indexOf('@') - 1;
    String regex = "^[\\w\\.+]*[\\w\\.]{3}\\@([\\w]+\\.)+[\\w]+[\\w]$";
    String regexOneUnderScoreandHyphen = "^[^_-].*[^_-]$";
    if(email.matches(regex) && !Character.isDigit(email.charAt(0)) && domainLength >= 3 && email.matches(regexOneUnderScoreandHyphen) && !email.equals(null) 
    		&& email.equals(email.toLowerCase()) ) 
    {
    	return true;
	     }
		else 
		{
			return false;
		}
	
}
}
