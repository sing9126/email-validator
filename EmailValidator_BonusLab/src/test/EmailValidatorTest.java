package test;

import static org.junit.Assert.*;

import org.junit.Test;

import sheridan.EmailValidator;

public class EmailValidatorTest 
{

	@Test
	public void testIsValidEmailRegular() {
		boolean result = EmailValidator.isValidEmail("john56@mail.com");
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailBoundaryIn() {
		boolean result = EmailValidator.isValidEmail("joh@mal.co");
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailBoundaryOut() {
		boolean result = EmailValidator.isValidEmail("jo@mal.co");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionSpaces() {
		boolean result = EmailValidator.isValidEmail("   @   .com");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionStartWithNumber() {
		boolean result = EmailValidator.isValidEmail("2john@mail.com");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionAccountName() {
		boolean result = EmailValidator.isValidEmail("hn@mail.com");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionDomain() {
		boolean result = EmailValidator.isValidEmail("john@ml.com");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionExtension() {
		boolean result = EmailValidator.isValidEmail("john@mail.c");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionHyphens() {
		boolean result = EmailValidator.isValidEmail("john@---.com");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionUnderscores() {
		boolean result = EmailValidator.isValidEmail("_____@____.com");
		assertFalse("valid email", result);
	}
	
	@Test  (expected=NullPointerException.class)
	public void testIsValidEmailExceptionNull() {
		boolean result = EmailValidator.isValidEmail(null);
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionsTwoSymbols() {
		boolean result = EmailValidator.isValidEmail("john@main@mail.com");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionsCapitalLetters() {
		boolean result = EmailValidator.isValidEmail("John@mail.com");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionsNumberinExtension() {
		boolean result = EmailValidator.isValidEmail("John@mail.c5");
		assertFalse("valid email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionOtherSymbols() {
		boolean result = EmailValidator.isValidEmail("John@mai%l.c5");
		assertFalse("valid email", result);
	}


}
